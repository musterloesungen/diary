#!/bin/bash

DIARY_FILE="diary.txt"

add_entry() {
    echo "Geben Sie Ihren Tagebucheintrag ein. Beenden Sie mit CTRL+D:"
    echo -e "\n--- $(date) ---" >> "$DIARY_FILE"
    cat >> "$DIARY_FILE"
    echo "Eintrag gespeichert."
}

view_entries() {
    local num=${1:-"all"}
    if [[ $num == "all" ]]; then
        cat "$DIARY_FILE"
    else
        tail -n "$(( $num * 2 ))" "$DIARY_FILE"
    fi
}

search_entries() {
    echo "Geben Sie den Suchbegriff ein:"
    xargs -I {} grep --color=always -C 1 {} "$DIARY_FILE"
}

show_help() {
    echo "Usage: $0 [OPTION]"
    echo "-a       Neuen Tagebucheintrag hinzufügen"
    echo "-v [NUM] Tagebucheinträge anzeigen (optional: die letzten [NUM] Einträge)"
    echo "-s       Tagebucheinträge durchsuchen"
    echo "-h       Diese Hilfe anzeigen und beenden"
}

while getopts ":av::sh" opt; do
    case ${opt} in
        a)
        add_entry
        ;;
        v)
        view_entries "$OPTARG"
        ;;
        s)
        search_entries
        ;;
        h)
        show_help
        exit 0
        ;;
        \?)
        echo "Unbekannte Option: -$OPTARG"
        show_help
        exit 1
        ;;
        :)
        echo "Option -$OPTARG erfordert ein Argument."
        exit 1
        ;;
    esac
done

# Falls kein Argument gegeben wurde, Hilfemenü anzeigen
if [[ $OPTIND -eq 1 ]]; then
    show_help
fi
