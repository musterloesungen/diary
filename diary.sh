#!/bin/bash

DIARY_FILE="diary.txt"

# Funktion zum Hinzufügen eines Tagebucheintrags
add_entry() {
    echo "Geben Sie Ihren Tagebucheintrag ein. Beenden Sie mit CTRL+D:"
    # Zeitstempel hinzufügen
    echo -e "\n--- $(date) ---" >> "$DIARY_FILE"
    # Benutzereingabe an die Datei anhängen
    cat >> "$DIARY_FILE"
    echo "Eintrag gespeichert."
}

# Funktion zum Anzeigen von Tagebucheinträgen
view_entries() {
    local num=${1:-"all"}
    if [[ $num == "all" ]]; then
        cat "$DIARY_FILE"
    else
        tail -n "$(( $num * 2 ))" "$DIARY_FILE"
    fi
}

# Funktion zum Durchsuchen von Tagebucheinträgen
search_entries() {
    local term=$1
    grep --color=always -C 1 "$term" "$DIARY_FILE"
}

# Funktion zum Anzeigen des Hilfemenüs
show_help() {
    echo "Usage: $0 [OPTION]"
    echo "-a, --add       Neuen Tagebucheintrag hinzufügen"
    echo "-v [NUM], --view [NUM]   Tagebucheinträge anzeigen (optional: die letzten [NUM] Einträge)"
    echo "-s TERM, --search TERM  Tagebucheinträge nach TERM durchsuchen"
    echo "-h, --help      Diese Hilfe anzeigen und beenden"
}

# Argumentverarbeitung
while [[ "$#" -gt 0 ]]; do
    key="$1"
    case $key in
        -a|--add)
        add_entry
        shift
        ;;
        -v|--view)
        if [[ "$2" =~ ^[0-9]+$ ]]; then
            view_entries "$2"
            shift
        else
            view_entries
        fi
        shift
        ;;
        -s|--search)
        search_entries "$2"
        shift
        shift
        ;;
        -h|--help)
        show_help
        exit 0
        ;;
        *)
        echo "Unbekannte Option: $key"
        show_help
        exit 1
        ;;
    esac
done

# Falls kein Argument gegeben wurde, Hilfemenü anzeigen
if [[ $# -eq 0 ]]; then
    show_help
fi
