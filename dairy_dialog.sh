#!/bin/bash

DIARY_FILE="diary.txt"

add_entry() {
    ENTRY=$(dialog --title "Neuer Tagebucheintrag" --inputbox "Geben Sie Ihren Tagebucheintrag ein:" 10 60 3>&1 1>&2 2>&3)
    if [ $? -eq 0 ]; then
        echo -e "\n--- $(date) ---\n$ENTRY" >> "$DIARY_FILE"
        dialog --title "Tagebuch" --msgbox "Eintrag gespeichert." 5 40
    fi
}

view_entries() {
    ENTRIES=$(cat "$DIARY_FILE")
    dialog --title "Ihr Tagebuch" --msgbox "$ENTRIES" 20 60
}

search_entries() {
    SEARCH_TERM=$(dialog --title "Suche" --inputbox "Geben Sie den Suchbegriff ein:" 10 60 3>&1 1>&2 2>&3)
    if [ $? -eq 0 ]; then
        RESULTS=$(grep -C 1 "$SEARCH_TERM" "$DIARY_FILE")
        dialog --title "Suchergebnisse" --msgbox "$RESULTS" 20 60
    fi
}

while true; do
    CHOICE=$(dialog --title "Tagebuch-Manager" --menu "Wählen Sie eine Aktion:" 15 60 4 \
    1 "Neuen Eintrag hinzufügen" \
    2 "Tagebucheinträge anzeigen" \
    3 "Tagebucheinträge durchsuchen" \
    4 "Beenden" \
    3>&1 1>&2 2>&3)

    case $CHOICE in
        1)
            add_entry
            ;;
        2)
            view_entries
            ;;
        3)
            search_entries
            ;;
        4)
            clear
            exit
            ;;
        *)
            dialog --title "Tagebuch-Manager" --msgbox "Ungültige Auswahl. Bitte versuchen Sie es erneut." 5 50
            ;;
    esac
done
