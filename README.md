**Aufgabe: Ein einfaches Shell-Skript zur Verwaltung von Tagebucheinträgen**

**Ziel**: Erstellen Sie ein Shell-Skript namens `diary.sh`, mit dem Benutzer Tagebucheinträge erstellen, anzeigen und durchsuchen können.

**Anforderungen**:

1. **Erstellen von Einträgen**:
    - Bei Aufruf mit der Option `-a` oder `--add` sollte das Skript den Benutzer auffordern, einen Tagebucheintrag zu verfassen.
    - Der Eintrag sollte mit einem Zeitstempel und einem Zeilenumbruch versehen und dann an eine Datei namens `diary.txt` angehängt werden.

2. **Anzeigen von Einträgen**:
    - Bei Aufruf mit der Option `-v` oder `--view` sollte das Skript alle Einträge aus `diary.txt` anzeigen.
    - Zusätzlich sollte der Benutzer die Möglichkeit haben, die letzten `n` Einträge anzuzeigen, indem er `-v n` oder `--view n` verwendet, wobei `n` die Anzahl der anzuzeigenden Einträge ist.

3. **Durchsuchen von Einträgen**:
    - Bei Aufruf mit der Option `-s` oder `--search`, gefolgt von einem Suchbegriff, sollte das Skript alle Einträge durchsuchen und diejenigen anzeigen, die den Begriff enthalten.

4. **Hilfemenü**:
    - Bei Aufruf mit der Option `-h` oder `--help` sollte das Skript ein kurzes Hilfemenü mit einer Beschreibung aller verfügbaren Optionen anzeigen.

**Bonuspunkte**:

- Verwenden Sie Farben in der Konsolenausgabe zur Hervorhebung von Überschriften, Suchergebnissen usw.
- Erstellen Sie eine Backup-Option, die alle Tagebucheinträge in eine ZIP-Datei packt und mit dem aktuellen Datum als Dateinamen speichert.
- Verwenden Sie die `trap`-Funktion, um sicherzustellen, dass das Skript bei einem unerwarteten Beenden (z.B. durch STRG+C) ordnungsgemäß beendet wird.
- Implementieren Sie Fehlerbehandlung und entsprechende Meldungen für den Fall, dass die Datei `diary.txt` nicht gelesen oder beschrieben werden kann.
